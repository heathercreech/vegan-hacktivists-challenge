A response to the Vegan Hacktivists coding challenge. Written on Windows with XAMPP as the dev environment.

## Running the exercise

1. Install all the dependencies

```
    composer i
    npm i
```

2. Build the frontend code

```
    npm run dev
```

3. Create a database titled `q_a`

4. Build the database tables

```
    php artisan migrate:fresh
```

5. Serve the app

```
    php artisan serve
```

6. Go to `localhost:8000`
