<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Anonymous Vegan Q & A</title>
    <!-- Fonts -->
    <link href="https://unpkg.com/bootstrap@4.3.1/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <style>
        .question:not(:first-of-type) {
            margin-top: 16px;
            display: block;
        }
    </style>
</head>

<body>
    <main class="row mw-100 overflow-hidden justify-content-around" style="height: 100vh">
        <div
            class="col-9 col-md-4 col-lg-3 py-2 h-100 overflow-hidden {{$selectedQuestion === null ? 'd-flex' : 'd-none'}} d-md-flex flex-column">
            <h1 class="mb-4">Q & A</h1>
            <div id='question-input-area' data-token="{{ csrf_token() }}">
            </div>
            <div class='mt-5 d-flex overflow-auto flex-column'>
                <div class="flex-column">
                    @foreach ($questions as $question)
                    <a href="/questions/{{$question->id}}" class='question text-body'>
                        <section class="card w-100">
                            <div class="card-body w-100">
                                <h2 class="card-title">{{ $question->title }}</h2>
                                <p class='text-truncate overflow-hidden mw-100 text-muted m-0'>
                                    {{ $question->description }}</p>
                            </div>
                        </section>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        @if ($selectedQuestion !== null)

        <div class="col-md-6 col-10 py-2 h-100 d-flex overflow-auto flex-column">
            <h1 class="mb-5">
                {{ $selectedQuestion->title }}
            </h1>

            <p class='lead mb-5'>
                {{ $selectedQuestion->description }}
            </p>

            <form method='post' action='/api/question/{{$selectedQuestion->id}}/answer'>
                <div class="form-group">
                    <label for="text">
                        Answer
                    </label>
                    <textarea class="form-control" name="text" required minlength="5"></textarea>
                </div>
                @csrf
                <div class="d-flex justify-content-end form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

            <div class='mt-5 d-flex overflow-auto flex-column'>
                @foreach ($answers as $answer)
                <section class='px-3 py-4 bg-light'>
                    <h3 class="text-secondary h6">
                        By Anonymous
                    </h3>
                    <p class='m-0 px-3'>
                        {{$answer->text}}
                    </p>
                </section>
                @endforeach
            </div>
        </div>
        @endif
    </main>
    <script crossorigin src="https://unpkg.com/raw/bootstrap@4.3.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
</body>

</html>