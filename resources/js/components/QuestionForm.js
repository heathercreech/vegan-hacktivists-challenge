import React, { useState, useCallback } from "react";
import ReactDOM from "react-dom";
import last from "lodash/last";
import { randomTitleQuestionPairs } from "./constants";

function takeRandom(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

const QuestionForm = ({ csrf_token }) => {
    const [description, setDescription] = useState("");
    const [titlePlaceholder, descriptionPlaceholder] = takeRandom(
        randomTitleQuestionPairs
    );

    const validateDescription = useCallback(e => {
        if (e.target.value.length >= 5 && last(e.target.value) !== "?") {
            e.target.setCustomValidity(
                "Please end your question with a question mark."
            );
        } else {
            e.target.setCustomValidity("");
        }
    }, []);

    return (
        <form method="post" action="/api/question">
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                    placeholder={titlePlaceholder}
                    type="text"
                    className="form-control"
                    name="title"
                    required
                    minLength="5"
                />
            </div>
            <div className="form-group">
                <label htmlFor="description">Description</label>
                <textarea
                    name="description"
                    className="form-control"
                    required
                    minLength="5"
                    value={description}
                    placeholder={descriptionPlaceholder}
                    onChange={e => setDescription(e.target.value)}
                    onInput={validateDescription}
                />
            </div>
            <input type="hidden" name="_token" value={csrf_token} />
            <div className="form-group d-flex justify-content-end">
                <button
                    className="btn"
                    type="reset"
                    // Can't forget to clear our managed value
                    onClick={e => setDescription("")}
                >
                    Clear
                </button>
                <button className="btn btn-primary" type="submit">
                    Add
                </button>
            </div>
        </form>
    );
};

export default QuestionForm;

const root = document.getElementById("question-input-area");
if (root) {
    const csrf_token = root.getAttribute("data-token");
    ReactDOM.render(
        <QuestionForm csrf_token={csrf_token} />,
        document.getElementById("question-input-area")
    );
}
