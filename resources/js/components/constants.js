export const randomTitleQuestionPairs = [
    ["Mon Mothma", "Was her dress blue and black, or white and gold?"],
    ["Luke Skywalker", "Isn't he a tad short to be a stormtrooper?"],
    ["Wedge Antilles", "Why are there two?"]
];
