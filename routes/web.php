<?php

use Illuminate\Support\Facades\Route;
use App\Question;
use App\Http\Resources\Question as QuestionResource;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/questions/{questionId?}', 'Questions@index')->name('questions');
