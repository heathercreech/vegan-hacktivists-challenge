<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question as QuestionModel;
use Ramsey\Uuid\Uuid;

class Question extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $id = Uuid::uuid4();

        $question = new QuestionModel();
        $question->id = $id;
        $question->title = $data['title'];
        $question->description = $data['description'];

        $question->save();

        return redirect()->route('questions', ['questionId' => $id]);
    }
}
