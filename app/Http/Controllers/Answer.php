<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer as AnswerModel;
use App\Question;
use Ramsey\Uuid\Uuid;

class Answer extends Controller
{
    //
    public function store(Request $request)
    {
        $questionId = $request->route('questionId');
        $data = $request->validate([
            'text' => 'required'
        ]);

        if (Question::where('id', '=', $questionId)) {
            $answer = new AnswerModel();
            $answer->id = Uuid::uuid4();
            $answer->question_id = $questionId;
            $answer->text = $data['text'];

            $answer->save();
            return redirect()->route('questions', [
                'questionId' => $questionId
            ]);
        } else {
            return redirect();
        }
    }
}
