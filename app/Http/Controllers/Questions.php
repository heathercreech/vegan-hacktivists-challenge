<?php

namespace App\Http\Controllers;

use App\Answer as AnswerModel;
use App\Question as QuestionModel;
use Illuminate\Support\Facades\DB;

class Questions extends Controller
{
    public function index($questionId = null)
    {
        // $request->path('questionId');
        $questions = QuestionModel::orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        $question = null;
        $answers = null;

        if ($questionId !== null) {
            $question = QuestionModel::where('id', '=', $questionId)->first();
            $answers = AnswerModel::where('question_id', '=', $questionId)
                ->orderBy('created_at', 'asc')
                ->take(10)
                ->get();
        }

        return view('question', [
            'questions' => $questions,
            'selectedQuestion' => $question,
            'answers' => $answers
        ]);
    }
}
